package cn.schoolwow.sdk.aliyundrive;

import cn.schoolwow.sdk.aliyundrive.domain.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public interface ALiYunDriveAPI {
    /**
     * 设置Token
     * */
    void setToken(String token) throws IOException;

    /**
     * 获取用户信息
     * */
    ALiYunDriveUser getUser() throws IOException;

    /**
     * 获取容量信息
     * */
    ALiYunDriveSBox getSbox() throws IOException;

    /**
     * 查找用户文件列表
     * @param keyword 关键字
     * */
    List<ALiYunDriveFile> search(String keyword) throws IOException;

    /**
     * 获取文件夹信息
     * @param parentAliYunDriveFile 上级目录,为null则获取根目录文件信息
     * */
    List<ALiYunDriveFile> list(ALiYunDriveDirectory parentAliYunDriveDirectory) throws IOException;

    /**
     * 创建文件夹
     * @param parentAliYunDriveFile 上级目录,为null则在根目录创建
     * @param name 文件夹名称
     * */
    ALiYunDriveDirectory createWithFolders(ALiYunDriveDirectory parentAliYunDriveDirectory, String name) throws IOException;

    /**
     * 上传文件
     * @param parentAliYunDriveFile 文件夹/文件对象
     * @param filePath 文件路径
     * */
    ALiYunDriveFile uploadFile(ALiYunDriveDirectory parentAliYunDriveDirectory, String filePath) throws IOException;

    /**
     * 重命名文件夹/文件
     * @param parentAliYunDriveFile 文件夹/文件对象
     * @param name 文件夹新名称
     * */
    ALiYunDriveFile update(ALiYunDriveFile aLiYunDriveFile, String name) throws IOException;

    /**
     * 移动文件/文件夹
     * @param parentAliYunDriveFile 文件夹/文件对象
     * @param name 文件夹新名称
     * */
    void move(ALiYunDriveFile sourceALiYunDriveFile, ALiYunDriveDirectory targetALiYunDriveDirectory) throws IOException;

    /**
     * 查看回收站文件列表
     * @param aLiYunDriveFile 文件夹/文件对象
     * @return 异步任务id
     * */
    List<ALiYunDriveFile> listRecycle() throws IOException;

    /**
     * 移动到回收站
     * @param aLiYunDriveFile 文件夹/文件对象
     * @return 异步任务id
     * */
    String trash(ALiYunDriveFile aLiYunDriveFile) throws IOException;

    /**
     * 从回收站恢复
     * @param aLiYunDriveFile 文件夹/文件对象
     * @return 异步任务id
     * */
    String restore(ALiYunDriveFile aLiYunDriveFile) throws IOException;

    /**
     * 彻底删除指定文件
     * @param aLiYunDriveFile 文件夹/文件对象
     * */
    void delete(ALiYunDriveFile aLiYunDriveFile) throws IOException;

    /**
     * 批量删除文件
     * @param aLiYunDriveFiles 文件夹/文件对象
     * */
    void batchDelete(ALiYunDriveFile... aLiYunDriveFiles) throws IOException;

    /**
     * 分享文件
     * @param expireTime 链接有效截止日期(为null则为永久)
     * @param aLiYunDriveFile 分享文件列表
     * */
    List<ALiYunDriveShare> listShare() throws IOException;

    /**
     * 分享文件
     * @param expireTime 链接有效截止日期(为null则为永久)
     * @param aLiYunDriveFile 分享文件列表
     * */
    ALiYunDriveShare create(LocalDateTime expireTime, ALiYunDriveFile... aLiYunDriveFiles) throws IOException;

    /**
     * 更改分享有效期
     * @param expireTime 链接有效截止日期(为null则为永久)
     * @param aLiYunDriveFileShare 阿里云盘分享
     * */
    ALiYunDriveShare update(LocalDateTime expireTime, ALiYunDriveShare aLiYunDriveFileShare) throws IOException;

    /**
     * 取消分享
     * @param aLiYunDriveFileShare 阿里云盘分享
     * */
    void cancelShare(ALiYunDriveShare... aLiYunDriveFileShares) throws IOException;

    /**
     * 查看收藏文件列表
     * @param aLiYunDriveFile 收藏文件夹(为空则查看根目录)
     * */
    List<ALiYunDriveFile> listStarred() throws IOException;

    /**
     * 收藏文件
     * */
    ALiYunDriveFile starred(ALiYunDriveFile aLiYunDriveFile) throws IOException;

    /**
     * 取消收藏文件
     * */
    ALiYunDriveFile cancelStarred(ALiYunDriveFile aLiYunDriveFile) throws IOException;
}