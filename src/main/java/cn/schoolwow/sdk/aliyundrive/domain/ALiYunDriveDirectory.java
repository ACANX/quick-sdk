package cn.schoolwow.sdk.aliyundrive.domain;

import java.sql.Timestamp;

/**
 * 阿里云盘文件信息
 */
public class ALiYunDriveDirectory {
    /**
     * 区域id
     */
    public String domainId;
    /**
     * 盘id
     */
    public String driveId;
    /**
     * 文件id
     */
    public String fileId;
    /**
     * 文件名称
     */
    public String name;
    /**
     * 文件类型(folder,file)
     */
    public String type;
    /**
     * 是否隐藏
     */
    public boolean hidden;
    /**
     * 是否被分享
     */
    public boolean starred;
    /**
     * 状态(available)
     */
    public String status;
    /**
     * 上级目录id
     */
    public String parentFileId;
    /**
     * 加密模式
     */
    public String encryptMode;
    /**
     * 创建者类型
     */
    public String creatorType;
    /**
     * 创建者id
     */
    public String creatorId;
    /**
     * 上次修改者类型
     */
    public String lastModifierType;
    /**
     * 上次修改者id
     */
    public String lastModifierId;
    /**
     * 创建时间
     */
    public Timestamp createdAt;
    /**
     * 修改时间
     */
    public Timestamp updatedAt;

    /**当前文件是否是文件夹*/
    public boolean isDirectory(){
        return "folder".equals(type);
    }

    @Override
    public String toString() {
        return "\n{\n" +
                "区域id:" + domainId + "\n" +
                "盘id:" + driveId + "\n" +
                "文件id:" + fileId + "\n" +
                "文件名称:" + name + "\n" +
                "文件类型:" + type + "\n" +
                "是否隐藏:" + hidden + "\n" +
                "是否被分享:" + starred + "\n" +
                "状态:" + status + "\n" +
                "上级目录id:" + parentFileId + "\n" +
                "加密模式:" + encryptMode + "\n" +
                "创建者类型:" + creatorType + "\n" +
                "创建者id:" + creatorId + "\n" +
                "上次修改者类型:" + lastModifierType + "\n" +
                "上次修改者id:" + lastModifierId + "\n" +
                "创建时间:" + createdAt + "\n" +
                "修改时间:" + updatedAt + "\n" +
                "}\n";
    }
}