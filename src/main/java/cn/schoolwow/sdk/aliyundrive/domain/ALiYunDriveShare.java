package cn.schoolwow.sdk.aliyundrive.domain;

import java.sql.Timestamp;
import java.util.List;

public class ALiYunDriveShare {
    /**
     * 盘id
     */
    public String driveId;
    /**
     * 文件id
     */
    public String fileId;
    /**
     * 显示名称
     */
    public String displayName;
    /**
     * 是否过期
     */
    public boolean expired;
    /**
     * 过期时间
     */
    public String expiration;
    /**
     * 文件id列表
     */
    public List<String> fileIdList;
    /**
     * 分享id
     */
    public String shareId;
    /**
     * 分享名称
     */
    public String shareName;
    /**
     * 分享url
     */
    public String shareUrl;
    /**
     * 分享信息
     */
    public String shareMsg;
    /**
     * 分享密码
     */
    public String sharePwd;
    /**
     * 分享策略
     */
    public String sharePolicy;
    /**
     * 状态
     */
    public String status;
    /**
     * 下载次数
     */
    public int downloadCount;
    /**
     * 预览次数
     */
    public int previewCount;
    /**
     * 转存次数
     */
    public int saveCount;
    /**
     * 创建时间
     */
    public Timestamp createdAt;
    /**
     * 创建者
     */
    public String creator;
    /**
     * 更新时间
     */
    public Timestamp updatedAt;

    @Override
    public String toString() {
        return "\n{\n" +
                "盘id:" + driveId + "\n" +
                "文件id:" + fileId + "\n" +
                "是否过期:" + expired + "\n" +
                "过期时间:" + expiration + "\n" +
                "文件id列表:" + fileIdList + "\n" +
                "分享id:" + shareId + "\n" +
                "分享名称:" + shareName + "\n" +
                "分享url:" + shareUrl + "\n" +
                "分享信息:" + shareMsg + "\n" +
                "分享密码:" + sharePwd + "\n" +
                "分享策略:" + sharePolicy + "\n" +
                "状态:" + status + "\n" +
                "下载次数:" + downloadCount + "\n" +
                "预览次数:" + previewCount + "\n" +
                "转存次数:" + saveCount + "\n" +
                "创建时间:" + createdAt + "\n" +
                "创建者:" + creator + "\n" +
                "更新时间:" + updatedAt + "\n" +
                "}\n";
    }
}