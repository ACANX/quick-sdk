package cn.schoolwow.sdk.aliyundrive.domain;

import java.sql.Timestamp;

/**
 * 用户信息
 * */
public class ALiYunDriveUser {
    /**
     * 区域id
     */
    public String domainId;

    /**
     * 默认盘Id
     */
    public long defaultDriveId;

    /**
     * 用户id
     */
    public String userId;

    /**
     * 用户名
     */
    public String userName;

    /**
     * 用户头像
     */
    public String avatar;

    /**
     * 邮箱
     */
    public String email;

    /**
     * 昵称
     */
    public String nickName;

    /**
     * 手机号
     */
    public String phone;

    /**
     * 创建时间
     */
    public Timestamp created_at;

    /**
     * 更新时间
     */
    public Timestamp updated_at;

    @Override
    public String toString() {
        return "\n{\n" +
                "领域id:" + domainId + "\n" +
                "用户id:" + userId + "\n" +
                "用户名:" + userName + "\n" +
                "用户头像:" + avatar + "\n" +
                "邮箱:" + email + "\n" +
                "昵称:" + nickName + "\n" +
                "手机号:" + phone + "\n" +
                "默认driverId:" + defaultDriveId + "\n" +
                "创建时间:" + created_at + "\n" +
                "更新时间:" + updated_at + "\n" +
                "}\n";
    }
}