package cn.schoolwow.sdk.aliyundrive.domain;

/**
 * 阿里云盘文件信息
 */
public class ALiYunDriveFile extends ALiYunDriveDirectory {
    /**
     * 目录
     */
    private String category;
    /**
     * 上传id
     */
    private String uploadId;
    /**
     * 文件链接
     */
    private String url;
    /**
     * 文件大小
     */
    private int size;
    /**
     * 发布标记
     */
    private int punishFlag;
    /**
     * 文件哈希
     */
    private String contentHash;
    /**
     * 文件哈希类型
     */
    private String contentHashName;
    /**
     * 文件类型
     */
    private String contentType;
    /**
     * crc64哈希
     */
    private String crc64Hash;
    /**
     * 下载链接
     */
    private String downloadUrl;
    /**
     * 文件扩展名
     */
    private String fileExtension;
    /**
     * mime扩展名
     */
    private String mimeExtension;
    /**
     * mime类型
     */
    private String mimeType;

    @Override
    public String toString() {
        return "\n{\n" +
                "区域id:" + domainId + "\n" +
                "盘id:" + driveId + "\n" +
                "文件id:" + fileId + "\n" +
                "文件名称:" + name + "\n" +
                "文件类型:" + type + "\n" +
                "是否隐藏:" + hidden + "\n" +
                "是否被分享:" + starred + "\n" +
                "状态:" + status + "\n" +
                "上级目录id:" + parentFileId + "\n" +
                "加密模式:" + encryptMode + "\n" +
                "创建者类型:" + creatorType + "\n" +
                "创建者id:" + creatorId + "\n" +
                "上次修改者类型:" + lastModifierType + "\n" +
                "上次修改者id:" + lastModifierId + "\n" +
                "目录:" + category + "\n" +
                "上传id:" + uploadId + "\n" +
                "文件链接:" + url + "\n" +
                "文件大小:" + size + "\n" +
                "发布标记:" + punishFlag + "\n" +
                "文件哈希:" + contentHash + "\n" +
                "文件哈希类型:" + contentHashName + "\n" +
                "文件类型:" + contentType + "\n" +
                "crc64哈希:" + crc64Hash + "\n" +
                "下载链接:" + downloadUrl + "\n" +
                "文件扩展名:" + fileExtension + "\n" +
                "mime扩展名:" + mimeExtension + "\n" +
                "mime类型:" + mimeType + "\n" +
                "创建时间:" + createdAt + "\n" +
                "修改时间:" + updatedAt + "\n" +
                "}\n";
    }
}