package cn.schoolwow.sdk.aliyundrive.domain;

/**
 * 容量信息
 */
public class ALiYunDriveSBox {
    /**
     * 盘id
     */
    public String driveId;
    /**
     * 已用空间大小
     */
    public int sboxUsedSize;
    /**
     * 实际使用空间大小
     */
    public int sboxRealUsedSize;
    /**
     * 总空间大小
     */
    public long sboxTotalSize;
    /**
     * 锁定状态
     */
    public boolean locked;
    /**
     * 是否设置了安全码
     */
    public boolean pinSetup;
    /**
     * 是否开启了保险箱
     */
    public boolean insuranceEnabled;

    @Override
    public String toString() {
        return "\n{\n" +
                "盘id:" + driveId + "\n" +
                "已用空间大小:" + sboxUsedSize + "\n" +
                "实际使用空间大小:" + sboxRealUsedSize + "\n" +
                "总空间大小:" + sboxTotalSize + "\n" +
                "锁定状态:" + locked + "\n" +
                "是否设置了安全码:" + pinSetup + "\n" +
                "是否开启了保险箱:" + insuranceEnabled + "\n" +
                "}\n";
    }
}