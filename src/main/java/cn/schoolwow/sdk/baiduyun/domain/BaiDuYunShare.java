package cn.schoolwow.sdk.baiduyun.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 百度云分享链接
 */
public class BaiDuYunShare {
    /**
     * url
     */
    public String url;
    /**
     * 分享url
     */
    public String surl;
    /**
     * uk
     */
    public String uk;
    /**
     * 分享id
     */
    public String shareId;
    /**
     * 签名
     */
    public String sign;
    /**
     * bdstoken
     */
    public String bdstoken;
    /**
     * 时间戳
     */
    public long timestamp;
    /**
     * 创建时间
     */
    public LocalDateTime ctime;
    /**
     * 照片
     */
    public String photo;
    /**
     * 关联用户名
     */
    public String linkUserName;
    /**
     * sharesuk
     */
    public String sharesuk;
    /**
     * 是否为私密分享
     */
    public boolean publicShare;
    /**
     * 额外信息
     */
    public String extra;
    /**
     * 百度云文件列表
     */
    public List<BaiDuYunFile> baiDuYunFileList = new ArrayList<>();

    @Override
    public String toString() {
        return "\n{\n" +
                "url:" + url + "\n"
                + "分享url:" + surl + "\n"
                + "uk:" + uk + "\n"
                + "分享id:" + shareId + "\n"
                + "签名:" + sign + "\n"
                + "bdstoken:" + bdstoken + "\n"
                + "时间戳:" + timestamp + "\n"
                + "创建时间:" + ctime + "\n"
                + "照片:" + photo + "\n"
                + "关联用户名:" + linkUserName + "\n"
                + "sharesuk:" + sharesuk + "\n"
                + "是否为私密分享:" + publicShare + "\n"
                + "额外信息:" + extra + "\n"
                + "百度云文件列表:" + baiDuYunFileList + "\n"
                + "}\n";
    }
}
