package cn.schoolwow.sdk.baiduyun.domain;

/**
 * 百度云用户信息
 */
public class BaiDuYunUser {
    /**
     * 邮箱
     */
    public String mail;
    /**
     * 手机
     */
    public String mobi;

    @Override
    public String toString() {
        return "\n{\n" +
                "邮箱:" + mail + "\n"
                + "手机:" + mobi + "\n"
                + "}\n";
    }
}
