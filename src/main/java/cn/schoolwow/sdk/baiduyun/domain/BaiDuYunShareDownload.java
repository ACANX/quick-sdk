package cn.schoolwow.sdk.baiduyun.domain;

import cn.schoolwow.quickhttp.request.Request;

/**
 * 百度云文件下载信息
 */
public class BaiDuYunShareDownload extends BaiDuYunFile {
    /**
     * 直接下载链接
     */
    public Request dlink;
    /**
     * 链接参数
     */
    public String context;

    @Override
    public String toString() {
        return "\n{\n" +
                "直接下载链接:" + dlink + "\n"
                + "链接参数:" + context + "\n"
                + "}\n";
    }
}
