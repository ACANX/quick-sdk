package cn.schoolwow.sdk.baiduyun.domain;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 百度云异常
 */
public class BaiDuYunException extends IOException {
    private static Map<Integer, String> errorMap = new HashMap<>();

    static {
        errorMap.put(-1, "由于您分享了违反相关法律法规的文件，分享功能已被禁用，之前分享出去的文件不受影响。");
        errorMap.put(-2, "用户不存在,请刷新页面后重试");
        errorMap.put(-3, "文件不存在,请刷新页面后重试");
        errorMap.put(-4, "登录信息有误，请重新登录试试");
        errorMap.put(-5, "host_key和user_key无效");
        errorMap.put(-6, "请重新登录");
        errorMap.put(-7, "该分享已删除或已取消");
        errorMap.put(-8, "该分享已经过期");
        errorMap.put(-9, "访问密码错误");
        errorMap.put(-10, "分享外链已经达到最大上限100000条，不能再次分享");
        errorMap.put(-11, "验证cookie无效");
        errorMap.put(-12, "设备尚未注册");
        errorMap.put(-13, "设备已经被绑定");
        errorMap.put(-14, "对不起，短信分享每天限制20条，你今天已经分享完，请明天再来分享吧！");
        errorMap.put(-15, "对不起，邮件分享每天限制20封，你今天已经分享完，请明天再来分享吧！");
        errorMap.put(-16, "对不起，该文件已经限制分享！");
        errorMap.put(-17, "文件分享超过限制");
        errorMap.put(-19, "请输入验证码");
        errorMap.put(-21, "预置文件无法进行相关操作");
        errorMap.put(-22, "被分享的文件无法重命名，移动等操作");
        errorMap.put(-23, "数据库操作失败，请联系netdisk管理员");
        errorMap.put(-24, "要取消的文件列表中含有不允许取消public的文件。");
        errorMap.put(-25, "非公测用户");
        errorMap.put(-26, "邀请码失效");
        errorMap.put(-30, "文件已存在");
        errorMap.put(-31, "文件保存失败");
        errorMap.put(-33, "一次支持操作999个，减点试试吧");
        errorMap.put(-32, "你的空间不足了哟");
        errorMap.put(-62, "当前页面(Cookie)已过期,请刷新页面后重试!");
        errorMap.put(-70, "你分享的文件中包含病毒或疑似病毒，为了你和他人的数据安全，换个文件分享吧");
        errorMap.put(-102, "云冲印文件7日内无法删除");
        errorMap.put(1, "服务器错误");
        errorMap.put(2, "接口请求错误，请稍候重试");
        errorMap.put(3, "一次操作文件不可超过100个");
        errorMap.put(4, "新文件名错误");
        errorMap.put(5, "目标目录非法");
        errorMap.put(6, "备用");
        errorMap.put(7, "NS非法或无权访问");
        errorMap.put(8, "ID非法或无权访问");
        errorMap.put(9, "申请key失败");
        errorMap.put(10, "创建文件的superfile失败");
        errorMap.put(11, "user_id(或user_name)非法或不存在");
        errorMap.put(12, "部分文件已存在于目标文件夹中");
        errorMap.put(13, "此目录无法共享");
        errorMap.put(14, "系统错误");
        errorMap.put(15, "操作失败");
        errorMap.put(102, "无权限操作该目录");
        errorMap.put(103, "提取码错误");
        errorMap.put(104, "验证cookie无效");
        errorMap.put(105, "surl参数有误");
        errorMap.put(110, "分享次数超出限制，可以到“我的分享”中查看已分享的文件链接");
        errorMap.put(111, "当前还有未完成的任务，需完成后才能操作");
        errorMap.put(112, "页面已过期");
        errorMap.put(114, "当前任务不存在，保存失败");
        errorMap.put(115, "该文件禁止分享");
        errorMap.put(118, "加密文件,extra参数错误");
        errorMap.put(121, "网盘文件过多，请删至500万个以内再试");
        errorMap.put(132, "删除文件需要验证您的身份");
        errorMap.put(142, "您已被移出当前共享目录，无法继续操作");
        errorMap.put(301, "其他请求出错");
        errorMap.put(404, "秒传md5不匹配 rapidupload 错误码");
        errorMap.put(406, "秒传创建文件失败 rapidupload 错误码");
        errorMap.put(407, "fileModify接口返回错误，未返回requestid rapidupload 错误码");
        errorMap.put(501, "获取的LIST格式非法");
        errorMap.put(600, "json解析出错");
        errorMap.put(601, "exception抛出异常");
        errorMap.put(617, "getFilelist其他错误");
        errorMap.put(618, "请求curl返回失败");
        errorMap.put(619, "pcs返回错误码");
        errorMap.put(9100, "你的帐号存在违规行为，已被冻结");
        errorMap.put(9200, "你的帐号存在违规行为，已被冻结");
        errorMap.put(9300, "你的帐号存在违规行为，该功能暂被冻结");
        errorMap.put(9400, "你的帐号异常，需验证后才能使用该功能");
        errorMap.put(9500, "你的帐号存在安全风险，已进入保护模式，请修改密码后使用");
    }

    /**
     * 异常码
     */
    private int errno;
    /**
     * 异常信息
     */
    private String errMsg;

    public BaiDuYunException(int errno, String errMsg) {
        super("错误码:" + errno + "," + ((errMsg == null || errMsg.isEmpty()) ? errorMap.get(errno) : errMsg));
        errMsg = (errMsg == null ? errorMap.get(errno) : errMsg);
        this.errno = errno;
        this.errMsg = errMsg;
    }

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
