package cn.schoolwow.sdk.baiduyun.domain;

import java.util.Date;

/**
 * 百度云文件分享链接信息
 */
public class BaiDuYunShareLink {
    /**
     * 链接
     */
    public String link;
    /**
     * 短链接
     */
    public String shorturl;
    /**
     * 文件id
     */
    public String fsId;
    /**
     * 分享id
     */
    public String shareid;
    /**
     * 创建时间
     */
    public Date ctime;
    /**
     * 过期类型
     */
    public int expiredType;

    @Override
    public String toString() {
        return "\n{\n" +
                "链接:" + link + "\n"
                + "短链接:" + shorturl + "\n"
                + "文件id:" + fsId + "\n"
                + "分享id:" + shareid + "\n"
                + "创建时间:" + ctime + "\n"
                + "过期类型:" + expiredType + "\n"
                + "}\n";
    }
}
