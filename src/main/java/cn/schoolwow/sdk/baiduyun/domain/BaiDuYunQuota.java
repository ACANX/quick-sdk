package cn.schoolwow.sdk.baiduyun.domain;

/**
 * 百度云空间占用信息
 */
public class BaiDuYunQuota {
    /**
     * 剩余可用空间
     */
    public long free;
    /**
     * 已使用空间
     */
    public long used;
    /**
     * 总空间大小
     */
    public long total;

    @Override
    public String toString() {
        return "\n{\n" +
                "剩余可用空间:" + free + "\n"
                + "已使用空间:" + used + "\n"
                + "总空间大小:" + total + "\n"
                + "}\n";
    }
}
