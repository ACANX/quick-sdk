package cn.schoolwow.sdk.baiduyun.domain;

import java.time.LocalDateTime;

/**
 * 百度云文件信息
 */
public class BaiDuYunFile {
    /**
     * 目录名
     */
    public String category;
    /**
     * 文件id
     */
    public String fsId;
    /**
     * 是否为文件夹
     */
    public boolean isDir;
    /**
     * md5
     */
    public String md5;
    /**
     * 路径
     */
    public String path;
    /**
     * 文件大小
     */
    public long size;
    /**
     * 本地创建时间
     */
    public LocalDateTime localCTime;
    /**
     * 服务端创建时间
     */
    public LocalDateTime serverCTime;
    /**
     * 服务端文件名
     */
    public String serverFileName;
    /**
     * 关联分享链接信息
     */
    public transient BaiDuYunShare baiDuYunShare;

    @Override
    public String toString() {
        return "\n{\n" +
                "目录名:" + category + "\n"
                + "文件id:" + fsId + "\n"
                + "是否为文件夹:" + isDir + "\n"
                + "md5:" + md5 + "\n"
                + "路径:" + path + "\n"
                + "文件大小:" + size + "\n"
                + "本地创建时间:" + localCTime + "\n"
                + "服务端创建时间:" + serverCTime + "\n"
                + "服务端文件名:" + serverFileName + "\n"
                + "}\n";
    }
}
