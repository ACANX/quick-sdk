package cn.schoolwow.sdk.baiduyun;

import cn.schoolwow.quickhttp.request.Request;
import cn.schoolwow.sdk.baiduyun.domain.*;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

/**
 * 百度云SDK
 */
public interface BaiDuYunAPI {
    /**
     * 设置百度云Cookie
     * @param cookieString pan.baidu.com域名下的所有Cookie信息
     * */
    void setCookie(String cookieString) throws IOException;

    /**
     * 获取用户登录信息
     */
    BaiDuYunUser getBaiDuYunUser() throws IOException;

    /**
     * 获取用户空间容量
     */
    BaiDuYunQuota quota() throws IOException;

    /**
     * 解析百度云分享链接
     * @param url 百度云分享链接,若有密码则在链接末尾加上{#密码}
     */
    BaiDuYunShare verify(String url) throws IOException;

    /**
     * 获取百度分享文件列表
     * @param baiDuYunShare 百度云分享
     * @param baiDuYunFile 百度网盘文件
     */
    List<BaiDuYunFile> shareList(BaiDuYunShare baiDuYunShare, BaiDuYunFile baiDuYunFile) throws IOException;

    /**
     * 遍历百度云分享文件夹
     * @param baiDuYunShare 百度云分享
     * @param baiDuYunFileConsumer 百度网盘文件遍历
     */
    void walkBaiDuYunShare(BaiDuYunShare baiDuYunShare, Consumer<BaiDuYunFile> baiDuYunFileConsumer) throws IOException;

    /**
     * 获取分享文件下载地址
     */
    BaiDuYunShareDownload sharedownload(BaiDuYunFile baiDuYunFile) throws IOException;

    /**
     * 保存到网盘
     */
    String transfer(BaiDuYunShare baiDuYunShare, String path) throws IOException;

    /**
     * 创建文件夹
     */
    String create(String path) throws IOException;

    /**
     * 上传文件
     * @param remoteDirectoryPath 远程文件夹路径
     * @param filePath 本地上传文件路径
     */
    BaiDuYunFile uploadFile(String remoteDirectoryPath, String filePath) throws IOException;

    /**
     * 获取百度云文件列表
     */
    List<BaiDuYunFile> list(String dir) throws IOException;

    /**
     * 搜索网盘内文件
     *
     * @param key 关键字
     */
    List<BaiDuYunFile> search(String key) throws IOException;

    /**
     * 直接下载网盘文件
     */
    @Deprecated
    String download(BaiDuYunFile baiDuYunFile) throws IOException;

    /**
     * pcs方式下载网盘内文件
     */
    Request pcsDownload(BaiDuYunFile baiDuYunFile) throws IOException;

    /**
     * client方式下载网盘内文件
     */
    Request clientDownload(BaiDuYunFile baiDuYunFile) throws IOException;

    /**
     * 获取百度云文件分享列表
     */
    List<BaiDuYunShareLink> record() throws IOException;

    /**
     * 私密分享百度云文件
     *
     * @param period 过期类型,可选值为0(永久),1(1天),7(7天)
     * @param pwd    提取码
     */
    BaiDuYunShareLink share(BaiDuYunFile baiDuYunFile, int period, String pwd) throws IOException;

    /**
     * 公开分享百度云文件
     *
     * @param period 过期类型,可选值为0(永久),1(1天),7(7天)
     */
    BaiDuYunShareLink pshare(BaiDuYunFile baiDuYunFile, int period) throws IOException;

    /**
     * 取消百度云文件分享列表
     */
    void cancel(BaiDuYunShareLink... baiDuYunShareLinks) throws IOException;

    /**
     * 重命名文件
     *
     * @param baiDuYunFile 百度云文件
     * @param newName      新文件名
     */
    void renameFile(BaiDuYunFile baiDuYunFile, String newName) throws IOException;

    /**
     * 删除文件
     *
     * @param baiDuYunFiles 百度云文件
     */
    void delete(BaiDuYunFile... baiDuYunFiles) throws IOException;

    /**
     * 查看回收站文件列表
     */
    List<BaiDuYunFile> recycleList() throws IOException;

    /**
     * 恢复回收站文件
     *
     * @param baiDuYunFiles 百度云文件
     */
    void restore(BaiDuYunFile... baiDuYunFiles) throws IOException;

    /**
     * 彻底删除百度云文件
     *
     * @param baiDuYunFiles 百度云文件
     */
    void recycleDelete(BaiDuYunFile... baiDuYunFiles) throws IOException;

    /**
     * 清空回收站
     */
    void clear() throws IOException;
}