package cn.schoolwow.sdk.weiyun;

import cn.schoolwow.sdk.weiyun.domain.*;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

/**
 * 微云SDK
 */
public interface WeiYunAPI {
    /**
     * 设置微云Cookie
     * @param cookieString weiyun.com域名下的所有Cookie信息
     * */
    void setCookie(String cookieString);

    /**
     * Cookie是否已经过期
     */
    boolean hasCookieExpire() throws IOException;

    /**
     * 获取用户配置信息
     */
    UserConfig diskUserInfoGet() throws IOException;

    /**
     * 获取微云文件夹信息
     *
     * @param weiYunDirectory 微云文件夹,为null则表示根目录
     */
    WeiYunDirectory diskDirBatchList(WeiYunDirectory weiYunDirectory) throws IOException;

    /**
     * 创建文件夹
     *
     * @param parentDirectory 父文件夹
     * @param dirName         文件夹名称
     */
    WeiYunDirectoryCreate diskDirCreate(WeiYunDirectory parentDirectory, String dirName) throws IOException;

    /**
     * 删除文件或者文件夹
     *
     * @param weiYunDirectoryList 微云文件夹列表
     * @param weiYunFileList      微云文件列表
     */
    WeiYunDirectory diskDirFileBatchDeleteEx(List<WeiYunDirectory> weiYunDirectoryList, List<WeiYunFile> weiYunFileList) throws IOException;

    /**
     * 上传文件
     * @param weiYunDirectory 上传文件存放目录,为null则为根目录
     * @param filePath 本地文件路径
     * */
    WeiYunFile uploadFile(WeiYunDirectory weiYunDirectory,String filePath) throws IOException;

    /**
     * 下载单个文件
     *
     * @param weiYunFile 微云文件
     */
    WeiYunFileDownload diskFileBatchDownload(WeiYunFile weiYunFile) throws IOException;

    /**
     * 批量下载
     *
     * @param weiYunDirectoryList 微云文件夹列表
     * @param weiYunFileList      微云文件列表
     */
    WeiYunFileDownload diskFileBatchDownload(List<WeiYunDirectory> weiYunDirectoryList, List<WeiYunFile> weiYunFileList) throws IOException;

    /**
     * 未登录情况下解析微云分享链接
     *
     * @param url 微云分享链接
     */
    WeiYunShareView weiyunShareViewNoLogin(String url) throws IOException;

    /**
     * 登录情况下解析微云分享链接
     *
     * @param url 微云分享链接
     */
    WeiYunShareView weiyunShareView(String url) throws IOException;

    /**
     * 解析微云分享链接,获取分享文件夹信息
     *
     * @param weiYunShareView 微云分享链接对象
     * @param weiYunDirectory 文件夹信息
     */
    void weiyunShareDirList(WeiYunShareView weiYunShareView, WeiYunDirectory weiYunDirectory) throws IOException;

    /**
     * 遍历微云文件夹
     * @param weiYunShareView 微云分享文件夹
     * @param weiYunDirectoryFunction 微云文件夹遍历,参数为微云文件夹,返回值为是否继续遍历
     */
    void walkWeiYunShareViewWeiYunFile(WeiYunShareView weiYunShareView, Consumer<WeiYunFile> weiYunFileConsumer) throws IOException;

    /**
     * 微云分享文件下载
     *
     * @param weiYunFile 微云分享链接对象
     */
    WeiYunFileDownload weiyunShareBatchDownload(WeiYunFile weiYunFile) throws IOException;

    /**
     * 保存到我的微云
     *
     * @param weiYunShareView 微云分享链接对象
     * @param weiYunDirectory 文件夹信息
     */
    WeiYunShareSave weiyunSharePartSaveData(WeiYunShareView weiYunShareView, WeiYunDirectory weiYunDirectory) throws IOException;

    /**
     * 查看我的分享链接列表
     */
    List<WeiYunShare> weiyunShareList() throws IOException;

    /**
     * 分享文件夹或者文件
     *
     * @param weiYunDirectoryList 微云文件夹列表
     * @param weiYunFileList      微云文件列表
     */
    WeiYunShare weiyunShareAddV2(List<WeiYunDirectory> weiYunDirectoryList, List<WeiYunFile> weiYunFileList) throws IOException;

    /**
     * 为分享文件加密
     *
     * @param weiYunShare 微云分享文件信息
     * @param pwd         指定分享链接密码
     */
    void weiyunSharePwdCreate(WeiYunShare weiYunShare, String pwd) throws IOException;

    /**
     * 修改分享文件密码
     *
     * @param weiYunShare 微云分享文件信息
     * @param pwd         指定分享链接密码
     */
    void weiyunSharePwdModify(WeiYunShare weiYunShare, String pwd) throws IOException;

    /**
     * 删除微云分享链接
     *
     * @param weiYunShareList 微云分享文件列表
     */
    List<WeiYunShareDelete> weiyunShareDelete(List<WeiYunShare> weiYunShareList) throws IOException;

    /**
     * 全局搜索文件夹和文件
     *
     * @param keyword 关键字
     */
    WeiYunDirectory fileSearchbyKeyWord(String keyword) throws IOException;

    /**
     * 获取回收站文件夹信息
     */
    WeiYunDirectory diskRecycleList() throws IOException;

    /**
     * 恢复回收站文件
     *
     * @param weiYunDirectoryList 微云文件夹列表
     * @param weiYunFileList      微云文件列表
     */
    WeiYunDirectoryDelete diskRecycleDirFileBatchRestore(List<WeiYunDirectory> weiYunDirectoryList, List<WeiYunFile> weiYunFileList) throws IOException;

    /**
     * 清空回收站
     */
    void diskRecycleClear() throws IOException;

    /**
     * 删除回收站文件夹信息
     *
     * @param weiYunDirectoryList 微云文件夹列表
     * @param weiYunFileList      微云文件列表
     */
    WeiYunDirectoryDelete diskRecycleDirFileClear(List<WeiYunDirectory> weiYunDirectoryList, List<WeiYunFile> weiYunFileList) throws IOException;
}
