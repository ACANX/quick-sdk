package cn.schoolwow.sdk.weiyun.domain;

/**
 * 微云分享删除结果
 */
public class WeiYunShareDelete {
    /**
     * 返回码
     */
    public int ret;
    /**
     * 分享key
     */
    public String shareKey;

    @Override
    public String toString() {
        return "\n{\n" +
                "返回码:" + ret + "\n"
                + "分享key:" + shareKey + "\n"
                + "}\n";
    }
}
