package cn.schoolwow.sdk.weiyun.domain;

import java.util.Date;

/**
 * 微云创建文件夹结果
 */
public class WeiYunDirectoryCreate {
    /**
     * 目录key
     */
    public String dirKey;
    /**
     * 目录名称
     */
    public String dirName;
    /**
     * 目录创建时间
     */
    public Date dirCtime;
    /**
     * 目录修改时间
     */
    public Date dirMtime;
    /**
     * 上级目录修改时间
     */
    public Date pdirMtime;
    /**
     * pdirKey
     */
    public String pdirKey;

    @Override
    public String toString() {
        return "\n{\n" +
                "目录key:" + dirKey + "\n"
                + "目录名称:" + dirName + "\n"
                + "目录创建时间:" + dirCtime + "\n"
                + "目录修改时间:" + dirMtime + "\n"
                + "上级目录修改时间:" + pdirMtime + "\n"
                + "pdirKey:" + pdirKey + "\n"
                + "}\n";
    }
}
