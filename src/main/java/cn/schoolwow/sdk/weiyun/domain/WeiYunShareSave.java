package cn.schoolwow.sdk.weiyun.domain;

/**
 * 微云分享保存结果
 */
public class WeiYunShareSave {
    /**
     * 下载统计信息
     */
    public String downloadCountInfo;
    /**
     * 保存会话key
     */
    public String saveSessionKey;

    @Override
    public String toString() {
        return "\n{\n" +
                "下载统计信息:" + downloadCountInfo + "\n"
                + "保存会话key:" + saveSessionKey + "\n"
                + "}\n";
    }
}
