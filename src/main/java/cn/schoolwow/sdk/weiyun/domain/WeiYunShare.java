package cn.schoolwow.sdk.weiyun.domain;

/**
 * 微云分享
 */
public class WeiYunShare {
    /**
     * 原始链接
     */
    public String rawUrl;
    /**
     * 短链接
     */
    public String shortUrl;
    /**
     * 分享名称
     */
    public String shareName;
    /**
     * 分享密码
     */
    public Object sharePwd;
    /**
     * 分享key
     */
    public String shareKey;
    /**
     * 缩略图链接
     */
    public String thumbUrl;

    @Override
    public String toString() {
        return "\n{\n" +
                "原始链接:" + rawUrl + "\n"
                + "短链接:" + shortUrl + "\n"
                + "分享名称:" + shareName + "\n"
                + "分享key:" + shareKey + "\n"
                + "缩略图链接:" + thumbUrl + "\n"
                + "}\n";
    }
}
