package cn.schoolwow.sdk.weiyun.domain;

/**
 * 用户配置信息
 */
public class UserConfig {
    /**
     * 昵称
     */
    public String nickName;
    /**
     * qq号
     */
    public String uin;
    /**
     * 总容量
     */
    public long totalSpace;
    /**
     * 已使用容量
     */
    public long usedSpace;
    /**
     * rootDirKey
     */
    public String rootDirKey;
    /**
     * mainDirKey
     */
    public String mainDirKey;

    @Override
    public String toString() {
        return "\n{\n" +
                "昵称:" + nickName + "\n"
                + "qq号:" + uin + "\n"
                + "已使用容量:" + usedSpace + "\n"
                + "总容量:" + totalSpace + "\n"
                + "rootDirKey:" + rootDirKey + "\n"
                + "mainDirKey:" + mainDirKey + "\n"
                + "}\n";
    }
}
