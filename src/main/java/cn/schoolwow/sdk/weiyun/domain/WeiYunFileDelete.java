package cn.schoolwow.sdk.weiyun.domain;

/**
 * 微云文件删除结果
 */
public class WeiYunFileDelete {
    /**
     * 返回码
     */
    public int retcode;
    /**
     * 返回结果
     */
    public String retmsg;
    /**
     * 回收站文件id
     */
    public String recycleFileId;
    /**
     * 恢复文件名称
     */
    public String restoreFilename;

    @Override
    public String toString() {
        return "\n{\n" +
                "返回码:" + retcode + "\n"
                + "返回结果:" + retmsg + "\n"
                + "回收站文件夹key:" + recycleFileId + "\n"
                + "恢复文件名称:" + restoreFilename + "\n"
                + "}\n";
    }
}
