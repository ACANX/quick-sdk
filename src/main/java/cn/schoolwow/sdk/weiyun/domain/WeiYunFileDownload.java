package cn.schoolwow.sdk.weiyun.domain;

import cn.schoolwow.quickhttp.request.Request;

import java.util.Date;

/**
 * 微云文件下载信息
 */
public class WeiYunFileDownload {
    /**
     * 返回码
     */
    public int retcode;
    /**
     * 返回结果
     */
    public String retmsg;
    /**
     * 编码url
     */
    public String encodeUrl;
    /**
     * 下载请求cookie名称
     */
    public String cookieName;
    /**
     * 下载请求cookie值
     */
    public String cookieValue;
    /**
     * 下载文件id
     */
    public String fileId;
    /**
     * 下载视频文件预览url
     */
    public String videoUrl;
    /**
     * 下载文件修改时间
     */
    public Date fileModifiedTime;
    /**
     * 下载文件内部下载ip
     */
    public String insideDownloadIP;
    /**
     * 下载文件外部下载ip
     */
    public String outsideDownloadIP;
    /**
     * 下载文件服务端名称
     */
    public String serverName;
    /**
     * 下载文件服务端端口
     */
    public int serverPort;
    /**
     * 下载文件链接
     */
    public String downloadUrl;
    /**
     * 下载文件服务端域名
     */
    public String httpsServerName;
    /**
     * 下载文件服务端端口
     */
    public int httpsServerPort;
    /**
     * 下载文件链接
     */
    public String httpsDownloadUrl;
    /**
     * 下载文件SHA
     */
    public String fileSha;
    /**
     * 下载文件日期
     */
    public Date fileVersion;
    /**
     * 下载文件大小
     */
    public long fileSize;
    /**
     * 下载文件缩略图链接
     */
    public String thumbUrl;
    /**
     * 下载文件缩略图链接
     */
    public String httpsThumbUrl;
    /**
     * 下载链接
     * */
    public Request dlink;

    @Override
    public String toString() {
        return "\n{\n" +
                "返回码:" + retcode + "\n"
                + "返回结果:" + retmsg + "\n"
                + "编码url:" + encodeUrl + "\n"
                + "请求cookie名称:" + cookieName + "\n"
                + "请求cookie值:" + cookieValue + "\n"
                + "文件id:" + fileId + "\n"
                + "视频预览url:" + videoUrl + "\n"
                + "修改时间:" + fileModifiedTime + "\n"
                + "内部下载ip:" + insideDownloadIP + "\n"
                + "外部下载ip:" + outsideDownloadIP + "\n"
                + "下载文件服务端名称:" + serverName + "\n"
                + "下载文件服务端端口:" + serverPort + "\n"
                + "下载文件链接:" + downloadUrl + "\n"
                + "下载文件服务端域名:" + httpsServerName + "\n"
                + "下载文件服务端端口:" + httpsServerPort + "\n"
                + "下载文件链接(https):" + httpsDownloadUrl + "\n"
                + "下载文件SHA:" + fileSha + "\n"
                + "下载文件日期:" + fileVersion + "\n"
                + "下载文件大小:" + fileSize + "\n"
                + "下载文件缩略图链接:" + thumbUrl + "\n"
                + "下载文件缩略图链接(https):" + httpsThumbUrl + "\n"
                + "}\n";
    }
}
