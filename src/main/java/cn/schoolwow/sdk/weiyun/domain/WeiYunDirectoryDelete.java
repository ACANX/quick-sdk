package cn.schoolwow.sdk.weiyun.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 微云文件夹删除结果
 */
public class WeiYunDirectoryDelete {
    /**
     * 返回码
     */
    public int retcode;
    /**
     * 返回结果
     */
    public String retmsg;
    /**
     * 回收站文件夹key
     */
    public String recycleDirKey;
    /**
     * 恢复文件夹名称
     */
    public String restoreDirName;
    /**
     * 删除文件列表
     */
    public List<WeiYunFileDelete> weiYunFileDeleteList = new ArrayList();
    /**
     * 删除目录列表
     */
    public List<WeiYunDirectoryDelete> weiYunDirectoryDeleteList = new ArrayList();

    @Override
    public String toString() {
        return "\n{\n" +
                "返回码:" + retcode + "\n"
                + "返回结果:" + retmsg + "\n"
                + "回收站文件夹key:" + recycleDirKey + "\n"
                + "恢复文件夹名称:" + restoreDirName + "\n"
                + "删除文件列表:" + weiYunFileDeleteList + "\n"
                + "删除目录列表:" + weiYunDirectoryDeleteList + "\n"
                + "}\n";
    }
}
