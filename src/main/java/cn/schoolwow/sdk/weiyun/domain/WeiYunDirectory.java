package cn.schoolwow.sdk.weiyun.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 微云文件夹
 */
public class WeiYunDirectory {
    /**
     * 目录key
     */
    public String dirKey;
    /**
     * 目录名称
     */
    public String dirName;
    /**
     * 目录创建时间
     */
    public Date dirCtime;
    /**
     * pdirKey
     */
    public String pdirKey;
    /**
     * ppdirKey
     */
    public String ppdirKey;
    /**
     * 占用空间
     */
    public long space;
    /**
     * 文件夹列表
     */
    public List<WeiYunDirectory> weiYunDirectoryList = new ArrayList<>();
    /**
     * 文件列表
     */
    public List<WeiYunFile> weiYunFileList = new ArrayList<>();

    @Override
    public String toString() {
        return "\n{\n" +
                "目录key:" + dirKey + "\n"
                + "目录名称:" + dirName + "\n"
                + "目录创建时间:" + dirCtime + "\n"
                + "总容量:" + space + "\n"
                + "pdirKey:" + pdirKey + "\n"
                + "ppdirKey:" + ppdirKey + "\n"
                + "目录列表:" + weiYunDirectoryList.toString() + "\n"
                + "文件列表:" + weiYunFileList.toString() + "\n"
                + "}\n";
    }
}
