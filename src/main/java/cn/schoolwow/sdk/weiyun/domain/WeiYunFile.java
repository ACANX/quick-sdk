package cn.schoolwow.sdk.weiyun.domain;

import java.util.Date;

/**
 * 微云文件
 */
public class WeiYunFile {
    /**
     * 文件id
     */
    public String fileId;
    /**
     * 文件名
     */
    public String fileName;
    /**
     * 文件后缀名
     */
    public String fileNameSuffix;
    /**
     * 文件大小
     */
    public long fileSize;
    /**
     * 文件创建时间
     */
    public Date fileCreatedTime;
    /**
     * 文件SHA
     */
    public String fileSha;
    /**
     * pdirKey
     */
    public String pdirKey;
    /**
     * ppdirKey
     */
    public String ppdirKey;
    /**
     * 关联微云分享
     * */
    public WeiYunShareView weiYunShareView;
    /**
     * 所属微云文件夹
     * */
    public WeiYunDirectory parentWeiYunDirectory;

    @Override
    public String toString() {
        return "\n{\n" +
                "文件id:" + fileId + "\n"
                + "文件名:" + fileName + "\n"
                + "文件后缀名:" + fileNameSuffix + "\n"
                + "文件大小:" + fileSize + "\n"
                + "文件创建时间:" + fileCreatedTime + "\n"
                + "文件指纹:" + fileSha + "\n"
                + "pdirKey:" + pdirKey + "\n"
                + "ppdirKey:" + ppdirKey + "\n"
                + "上级目录:" + (null==parentWeiYunDirectory?"无":parentWeiYunDirectory.dirName) + "\n"
                + "}\n";
    }
}
