package cn.schoolwow.sdk.weiyun.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 微云分享文件
 */
public class WeiYunShareView {
    /**
     * 分享者qq号
     */
    public String shareUin;
    /**
     * 分享者昵称
     */
    public String shareNickName;
    /**
     * 下载次数
     */
    public long downCnt;
    /**
     * 浏览次数
     */
    public long viewCnt;
    /**
     * 分享key
     */
    public String shareKey;
    /**
     * 分享链接密码
     */
    public String sharePwd = "";
    /**
     * 分享名称
     */
    public String shareName;
    /**
     * 创建时间
     */
    public Date fileCreatedTime;
    /**
     * pdirKey
     */
    public String pdirKey;
    /**
     * ppdirKey
     */
    public String ppdirKey;
    /**
     * 文件列表
     */
    public List<WeiYunFile> weiYunFileList = new ArrayList<>();
    /**
     * 文件夹列表
     */
    public List<WeiYunDirectory> weiYunDirectoryList = new ArrayList<>();

    @Override
    public String toString() {
        return "\n{\n" +
                "分享者qq号:" + shareUin + "\n"
                + "分享者昵称:" + shareNickName + "\n"
                + "下载次数:" + downCnt + "\n"
                + "浏览次数:" + viewCnt + "\n"
                + "分享key:" + shareKey + "\n"
                + "分享名称:" + shareName + "\n"
                + "创建时间:" + fileCreatedTime + "\n"
                + "pdirKey:" + pdirKey + "\n"
                + "ppdirKey:" + ppdirKey + "\n"
                + "文件列表:" + weiYunFileList + "\n"
                + "文件夹列表:" + weiYunDirectoryList + "\n"
                + "}\n";
    }
}
