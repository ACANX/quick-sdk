package cn.schoolwow.sdk.weiyun.exception;

import java.io.IOException;

/**
 * 微云文件异常
 */
public class WeiYunException extends IOException {
    /**
     * 异常码
     */
    private int retcode;
    /**
     * 异常信息
     */
    private String retmsg;

    public WeiYunException(int retcode, String retmsg) {
        super(retmsg);
        this.retcode = retcode;
        this.retmsg = retmsg;
    }

    public int getRetcode() {
        return retcode;
    }

    public void setRetcode(int retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }
}
