package cn.schoolwow.sdk.weiyun;

import cn.schoolwow.sdk.weiyun.domain.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class WeiYunAPITest {
    /**
     * 微云Cookie
     */
    private String cookieString = "{weiyun.com域名下Cookie}";
    /**
     * 微云实现类
     */
    private WeiYunAPI weiYunAPI = new WeiYunAPIImpl(cookieString);

    @Test
    public void hasCookieExpire() throws Exception {
        Assert.assertFalse(weiYunAPI.hasCookieExpire());
    }

    @Test
    public void getUserConfig() throws Exception {
        UserConfig userConfig = weiYunAPI.diskUserInfoGet();
        System.out.println(userConfig.toString());
        Assert.assertNotNull(userConfig.nickName);
        Assert.assertNotNull(userConfig.uin);
        Assert.assertNotNull(userConfig.rootDirKey);
        Assert.assertNotNull(userConfig.mainDirKey);
        Assert.assertTrue(userConfig.usedSpace > 0);
        Assert.assertTrue(userConfig.totalSpace > 0);
    }

    @Test
    public void diskDirBatchList() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskDirBatchList();
        System.out.println(weiYunDirectory);
        Assert.assertNotNull(weiYunDirectory.pdirKey);
    }

    @Test
    public void diskDirCreate() throws Exception {
        String dirName = "测试目录";
        WeiYunDirectoryCreate weiYunDirectoryCreate = weiYunAPI.diskDirCreate(weiYunAPI.diskDirBatchList(), dirName);
        System.out.println(weiYunDirectoryCreate);
        Assert.assertEquals(weiYunDirectoryCreate.dirName, dirName);
    }

    @Test
    public void diskDirFileBatchDeleteEx() throws Exception {
        WeiYunDirectory targetDirectory = null;
        for (WeiYunDirectory weiYunDirectory : weiYunAPI.diskDirBatchList().weiYunDirectoryList) {
            if ("测试目录".equals(weiYunDirectory.dirName)) {
                targetDirectory = weiYunDirectory;
                break;
            }
        }
        if (targetDirectory == null) {
            return;
        }
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskDirFileBatchDeleteEx(Arrays.asList(targetDirectory), null);
        System.out.println(weiYunDirectory);
        Assert.assertEquals(targetDirectory.dirKey, weiYunDirectory.weiYunDirectoryList.get(0).dirKey);
    }

    @Test
    public void diskFileBatchDownload() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskDirBatchList();
        WeiYunFileDownload weiYunFileDownload = weiYunAPI.diskFileBatchDownload(weiYunDirectory.weiYunFileList.get(0));
        System.out.println(weiYunFileDownload);
        Assert.assertNotNull(weiYunFileDownload.httpsDownloadUrl);
    }

    @Test
    public void weiyunShareView() throws Exception {
        String url = "{普通微云链接}";
        WeiYunShareView weiYunShare = weiYunAPI.weiyunShareView(url);
        System.out.println(weiYunShare);
        Assert.assertNotNull(weiYunShare.shareKey);
    }

    @Test
    public void weiyunShareViewPwd() throws Exception {
        String url = "{带密码的微云链接}";
        WeiYunShareView weiYunShare = weiYunAPI.weiyunShareView(url);
        System.out.println(weiYunShare);
        Assert.assertNotNull(weiYunShare.shareKey);
    }

    @Test
    @Ignore
    public void weiyunSharePartSaveData() throws Exception {
        WeiYunDirectory targetDirectory = null;
        for (WeiYunDirectory weiYunDirectory : weiYunAPI.diskDirBatchList().weiYunDirectoryList) {
            if ("测试目录".equals(weiYunDirectory.dirName)) {
                targetDirectory = weiYunDirectory;
                break;
            }
        }
        if (null == targetDirectory) {
            diskDirCreate();
            for (WeiYunDirectory weiYunDirectory : weiYunAPI.diskDirBatchList().weiYunDirectoryList) {
                if ("测试目录".equals(weiYunDirectory.dirName)) {
                    targetDirectory = weiYunDirectory;
                    break;
                }
            }
        }
        if (null == targetDirectory) {
            return;
        }
        String url = "{普通微云链接}";
        WeiYunShareView weiYunShare = weiYunAPI.weiyunShareView(url);
        System.out.println(weiYunShare);
        Assert.assertNotNull(weiYunShare.shareKey);
        WeiYunShareSave weiYunShareSave = weiYunAPI.weiyunSharePartSaveData(weiYunShare, targetDirectory);
        System.out.println(weiYunShareSave);
        Assert.assertNotNull(weiYunShareSave.downloadCountInfo);
    }

    @Test
    public void weiyunShareList() throws Exception {
        List<WeiYunShare> weiYunShareList = weiYunAPI.weiyunShareList();
        System.out.println(weiYunShareList);
        Assert.assertEquals(0, weiYunShareList.size());
    }

    @Test
    public void WeiyunShareAddV2() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskDirBatchList();
        WeiYunShare weiYunShare = weiYunAPI.weiyunShareAddV2(Arrays.asList(weiYunDirectory.weiYunDirectoryList.get(0)), null);
        System.out.println(weiYunShare);
        Assert.assertNotNull(weiYunShare.rawUrl);
    }

    @Test
    public void weiyunSharePwdCreate() throws Exception {
        List<WeiYunShare> weiYunShareList = weiYunAPI.weiyunShareList();
        System.out.println(weiYunShareList);
        if (!weiYunShareList.isEmpty()) {
            weiYunAPI.weiyunSharePwdCreate(weiYunShareList.get(0), "aaabbb");
            weiYunAPI.weiyunSharePwdModify(weiYunShareList.get(0), "cccddd");
        }
    }

    @Test
    public void weiyunShareDelete() throws Exception {
        List<WeiYunShare> weiYunShareList = weiYunAPI.weiyunShareList();
        if (weiYunShareList.isEmpty()) {
            weiyunSharePwdCreate();
            weiYunShareList = weiYunAPI.weiyunShareList();
        }
        System.out.println(weiYunShareList);
        List<WeiYunShareDelete> weiYunShareDeleteList = weiYunAPI.weiyunShareDelete(weiYunShareList);
        System.out.println(weiYunShareDeleteList);
        Assert.assertEquals(weiYunShareDeleteList.size(), weiYunShareList.size());
    }

    @Test
    public void fileSearchbyKeyWord() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.fileSearchbyKeyWord("测试目录");
        if (null == weiYunDirectory) {
            diskDirCreate();
            weiYunDirectory = weiYunAPI.fileSearchbyKeyWord("测试目录");
        }
        System.out.println(weiYunDirectory);
        Assert.assertEquals("测试目录", weiYunDirectory.dirName);
    }

    @Test
    public void diskRecycleList() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskRecycleList();
        System.out.println(weiYunDirectory);
        Assert.assertEquals(0, weiYunDirectory.weiYunFileList.size());
    }

    @Test
    public void diskRecycleDirFileBatchRestore() throws Exception {
        WeiYunDirectory targetDirectory = null;
        for (WeiYunDirectory weiYunDirectory : weiYunAPI.diskDirBatchList().weiYunDirectoryList) {
            if ("测试目录".equals(weiYunDirectory.dirName)) {
                targetDirectory = weiYunDirectory;
                break;
            }
        }
        if (targetDirectory == null) {
            diskDirCreate();
        }
        diskDirFileBatchDeleteEx();

        WeiYunDirectory recycleDirectory = weiYunAPI.diskRecycleList();
        WeiYunDirectoryDelete weiYunDirectoryDelete = weiYunAPI.diskRecycleDirFileBatchRestore(recycleDirectory.weiYunDirectoryList, null);
        System.out.println(weiYunDirectoryDelete);
        Assert.assertEquals("测试目录", weiYunDirectoryDelete.weiYunDirectoryDeleteList.get(0).restoreDirName);
    }

    @Test
    public void diskRecycleClear() throws Exception {
        weiYunAPI.diskRecycleClear();
    }

    @Test
    public void diskRecycleDirFileClear() throws Exception {
        WeiYunDirectory weiYunDirectory = weiYunAPI.diskRecycleList();
        WeiYunDirectoryDelete weiYunDirectoryDelete = weiYunAPI.diskRecycleDirFileClear(weiYunDirectory.weiYunDirectoryList, weiYunDirectory.weiYunFileList);
        System.out.println(weiYunDirectoryDelete);
        Assert.assertEquals(0, weiYunDirectoryDelete.weiYunFileDeleteList.size());
        Assert.assertEquals(0, weiYunDirectoryDelete.weiYunDirectoryDeleteList.size());
    }
}