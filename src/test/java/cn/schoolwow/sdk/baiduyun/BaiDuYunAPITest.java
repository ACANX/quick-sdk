package cn.schoolwow.sdk.baiduyun;

import cn.schoolwow.quickhttp.response.Response;
import cn.schoolwow.sdk.baiduyun.domain.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class BaiDuYunAPITest {
    /**
     * 百度云SDK实现类
     */
    private BaiDuYunAPI baiDuYunAPI = new BaiDuYunAPIImpl();
    {
        try {
            baiDuYunAPI.setCookie("{输入baidu.com域名下的所有Cookie}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String url = "{输入百度分享链接,密码添加到链接最后,链接#密码}";

    @Test
    public void getBaiDuYunUser() throws Exception {
        BaiDuYunUser baiDuYunUser = baiDuYunAPI.getBaiDuYunUser();
        System.out.println(baiDuYunUser);
        Assert.assertNotNull(baiDuYunUser.mail);
    }

    @Test
    public void quota() throws Exception {
        BaiDuYunQuota baiDuYunQuota = baiDuYunAPI.quota();
        System.out.println(baiDuYunQuota);
        Assert.assertTrue(baiDuYunQuota.free > 0);
        Assert.assertTrue(baiDuYunQuota.used > 0);
        Assert.assertTrue(baiDuYunQuota.total > 0);
    }

    @Test
    public void verify() throws Exception {
        BaiDuYunShare baiDuYunShare = baiDuYunAPI.verify(url);
        System.out.println(baiDuYunShare);
        Assert.assertNotNull(baiDuYunShare.shareId);
    }

    @Test
    public void transfer() throws Exception {
        BaiDuYunShare baiDuYunShare = baiDuYunAPI.verify(url);
        System.out.println(baiDuYunShare);
        String result = baiDuYunAPI.transfer(baiDuYunShare, "/du");
        System.out.println(result);
        Assert.assertNotNull(result);
    }

    @Test
    public void create() throws Exception {
        String result = baiDuYunAPI.create("/tmp");
        System.out.println(result);
        Assert.assertNotNull(result);
    }

    @Test
    public void list() throws Exception {
        List<BaiDuYunFile> baiDuYunFileList = baiDuYunAPI.list("/");
        System.out.println(baiDuYunFileList);
        Assert.assertTrue(baiDuYunFileList.size() > 0);
    }

    @Test
    public void search() throws Exception {
        List<BaiDuYunFile> baiDuYunFileList = baiDuYunAPI.search("*.msi");
        System.out.println(baiDuYunFileList);
        Assert.assertTrue(baiDuYunFileList.size() > 0);
    }

    @Test
    public void sharedownload() throws IOException {
        BaiDuYunShare baiDuYunShare = baiDuYunAPI.verify(url);
        BaiDuYunShareDownload baiDuYunShareDownload = baiDuYunAPI.sharedownload(baiDuYunShare.baiDuYunFileList.get(0));
        System.out.println(baiDuYunShareDownload);
        Assert.assertNotNull(baiDuYunShareDownload.dlink);
        Response response = baiDuYunShareDownload.dlink.execute();
        Assert.assertEquals(200,response.statusCode());
        Path path = Paths.get(System.getProperty("user.dir")+"/"+baiDuYunShareDownload.serverFileName);
        Files.deleteIfExists(path);
        response.bodyAsFile(path);
        Assert.assertEquals(response.contentLength(), Files.size(path));
        Files.deleteIfExists(path);
    }

    @Test
    public void download() throws Exception {
        BaiDuYunFile baiDuYunFile = getBaiDuYunFile();
        String dlink = baiDuYunAPI.download(baiDuYunFile);
        System.out.println(dlink);
        Assert.assertNotNull(dlink);
    }

    @Test
    public void pcsDownload() throws Exception {
        BaiDuYunFile baiDuYunFile = getBaiDuYunFile();
        Response response = baiDuYunAPI.pcsDownload(baiDuYunFile).execute();
        Assert.assertEquals(200, response.statusCode());
    }

    @Test
    public void clientDownload() throws Exception {
        BaiDuYunShare baiDuYunShare = baiDuYunAPI.verify(url);
        Response response = baiDuYunAPI.clientDownload(baiDuYunShare.baiDuYunFileList.get(0)).execute();
        Assert.assertEquals(200, response.statusCode());
    }

    @Test
    public void record() throws Exception {
        List<BaiDuYunShareLink> baiDuYunShareLinkList = baiDuYunAPI.record();
        System.out.println(baiDuYunShareLinkList);
    }

    @Test
    public void share() throws Exception {
        BaiDuYunFile baiDuYunFile = getBaiDuYunFile();
        BaiDuYunShareLink baiDuYunShareLink = baiDuYunAPI.share(baiDuYunFile, 7, "1234");
        baiDuYunAPI.cancel(baiDuYunShareLink);
    }

    @Test
    public void pshare() throws Exception {
        BaiDuYunFile baiDuYunFile = getBaiDuYunFile();
        BaiDuYunShareLink baiDuYunShareLink = baiDuYunAPI.pshare(baiDuYunFile, 7);
        baiDuYunAPI.cancel(baiDuYunShareLink);
    }

    @Test
    public void recycleList() throws Exception {
        List<BaiDuYunFile> baiDuYunFileList = baiDuYunAPI.recycleList();
        System.out.println(baiDuYunFileList);
        Assert.assertTrue(baiDuYunFileList.size() == 0);
    }

    @Test
    public void delete() throws Exception {
        BaiDuYunFile baiDuYunFile = getBaiDuYunFile();
        baiDuYunAPI.delete(baiDuYunFile);
        Thread.sleep(3000);
        baiDuYunAPI.restore(baiDuYunFile);
    }

    private BaiDuYunFile getBaiDuYunFile() throws IOException {
        List<BaiDuYunFile> baiDuYunFileList = baiDuYunAPI.list("/du");
        for (BaiDuYunFile baiDuYunFile : baiDuYunFileList) {
            if (baiDuYunFile.serverFileName.equals("iPlaySoft.com-雷鸟下载 v2.0.1.zip")) {
                return baiDuYunFile;
            }
        }
        return null;
    }
}