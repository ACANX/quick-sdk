package cn.schoolwow.sdk.aliyundrive;

import cn.schoolwow.sdk.aliyundrive.domain.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class ALiYunDriveAPITest {
    /**
     * 阿里云盘实现类
     */
    private ALiYunDriveAPI aLiYunDriveAPI = new ALiYunDriveAPIImpl("{Authorization认证头的值}");

    @Test
    public void getUser() throws Exception {
        ALiYunDriveUser aLiYunDriveUser = aLiYunDriveAPI.getUser();
        System.out.println(aLiYunDriveUser);
        Assert.assertNotNull(aLiYunDriveUser);
    }

    @Test
    public void getSbox() throws Exception {
        ALiYunDriveSBox aLiYunDriveSBox = aLiYunDriveAPI.getSbox();
        System.out.println(aLiYunDriveSBox);
        Assert.assertNotNull(aLiYunDriveSBox);
    }

    @Test
    public void search() throws Exception {
        List<ALiYunDriveFile> aLiYunDriveFileList = aLiYunDriveAPI.search("ALiYunDrive");
        System.out.println(aLiYunDriveFileList);
        Assert.assertNotNull(aLiYunDriveFileList);
        Assert.assertEquals(0,aLiYunDriveFileList.size());
    }

    @Test
    public void list() throws Exception {
        List<ALiYunDriveFile> aLiYunDriveFileList = aLiYunDriveAPI.list(null);
        System.out.println(aLiYunDriveFileList);
        Assert.assertNotNull(aLiYunDriveFileList);
        Assert.assertTrue(aLiYunDriveFileList.size()>0);
    }

    @Test
    public void createWithFolders() throws Exception {
        ALiYunDriveDirectory aLiYunDriveDirectory = aLiYunDriveAPI.createWithFolders(null,"新文件夹");
        System.out.println(aLiYunDriveDirectory);
        Assert.assertNotNull(aLiYunDriveDirectory);
    }

    @Test
    public void uploadFile() throws IOException {
        String filePath = System.getProperty("user.dir") + "/LICENSE";
        ALiYunDriveFile aLiYunDriveFile = aLiYunDriveAPI.uploadFile(null,filePath);
        System.out.println(aLiYunDriveFile);
        Assert.assertNotNull(aLiYunDriveFile);
    }

    @Test
    public void updateFileName() throws IOException {
        ALiYunDriveFile newDirectoryALiYunDriveFile = getALiYunDriveFile("新文件夹");
        ALiYunDriveFile aLiYunDriveFile = aLiYunDriveAPI.update(newDirectoryALiYunDriveFile,"新新文件夹");
        System.out.println(aLiYunDriveFile);
        Assert.assertNotNull(aLiYunDriveFile);
        Assert.assertEquals("新新文件夹",aLiYunDriveFile.name);
    }

    @Test
    public void move() throws IOException {
        ALiYunDriveFile licenseALiYunDriveFile = getALiYunDriveFile("LICENSE");
        ALiYunDriveFile newDirectoryALiYunDriveFile = getALiYunDriveFile("新文件夹");
        aLiYunDriveAPI.move(licenseALiYunDriveFile,newDirectoryALiYunDriveFile);
    }

    @Test
    public void trash() throws IOException {
        ALiYunDriveFile licenseALiYunDriveFile = getALiYunDriveFile("LICENSE");
        String asyncTaskId = aLiYunDriveAPI.trash(licenseALiYunDriveFile);
        System.out.println(asyncTaskId);
    }

    @Test
    public void restore() throws IOException {
        ALiYunDriveFile licenseALiYunDriveFile = getALiYunDriveFileFromRecycle("LICENSE");
        String asyncTaskId = aLiYunDriveAPI.restore(licenseALiYunDriveFile);
        System.out.println(asyncTaskId);
    }

    @Test
    public void delete() throws IOException {
        ALiYunDriveFile licenseALiYunDriveFile = getALiYunDriveFileFromRecycle("LICENSE");
        aLiYunDriveAPI.delete(licenseALiYunDriveFile);
    }

    @Test
    public void create() throws IOException {
        ALiYunDriveFile aLiYunDriveFile = getALiYunDriveFile("新新文件夹");
        ALiYunDriveShare aLiYunDriveShare = aLiYunDriveAPI.create(null,aLiYunDriveFile);
        System.out.println(aLiYunDriveShare);
        Assert.assertNotNull(aLiYunDriveShare);
    }

    @Test
    public void updateShare() throws IOException {
        ALiYunDriveShare aLiYunDriveShare = getALiYunDriveFileFromShare("新新文件夹");
        aLiYunDriveShare = aLiYunDriveAPI.update(LocalDateTime.now().plusDays(7),aLiYunDriveShare);
        System.out.println(aLiYunDriveShare);
        Assert.assertNotNull(aLiYunDriveShare);
    }

    @Test
    public void cancelShare() throws IOException {
        ALiYunDriveShare aLiYunDriveShare = getALiYunDriveFileFromShare("新新文件夹");
        aLiYunDriveAPI.cancelShare(aLiYunDriveShare);
    }

    @Test
    public void starred() throws IOException {
        ALiYunDriveFile aLiYunDriveFile = getALiYunDriveFile("新新文件夹");
        aLiYunDriveAPI.starred(aLiYunDriveFile);
    }

    @Test
    public void cancelStarred() throws IOException {
        ALiYunDriveFile aLiYunDriveFile = getALiYunDriveFileFromShareStarred("新新文件夹");
        aLiYunDriveAPI.cancelStarred(aLiYunDriveFile);
    }

    private ALiYunDriveFile getALiYunDriveFileFromShareStarred(String keyword) throws IOException {
        List<ALiYunDriveFile> aLiYunDriveFileList = aLiYunDriveAPI.listStarred();
        for(ALiYunDriveFile aLiYunDriveFile:aLiYunDriveFileList){
            if(aLiYunDriveFile.name.equals(keyword)){
                return aLiYunDriveFile;
            }
        }
        throw new IllegalArgumentException("收藏列表不存在该收藏!收藏文件名称:"+keyword);
    }

    private ALiYunDriveShare getALiYunDriveFileFromShare(String keyword) throws IOException {
        List<ALiYunDriveShare> aLiYunDriveShareList = aLiYunDriveAPI.listShare();
        for(ALiYunDriveShare aLiYunDriveShare:aLiYunDriveShareList){
            if(aLiYunDriveShare.displayName.equals(keyword)){
                return aLiYunDriveShare;
            }
        }
        throw new IllegalArgumentException("分享列表不存在该分享!分享名称:"+keyword);
    }

    private ALiYunDriveFile getALiYunDriveFileFromRecycle(String keyword) throws IOException {
        List<ALiYunDriveFile> aLiYunDriveFileList = aLiYunDriveAPI.listRecycle();
        for(ALiYunDriveFile aLiYunDriveFile:aLiYunDriveFileList){
            if(aLiYunDriveFile.name.equals(keyword)){
                return aLiYunDriveFile;
            }
        }
        throw new IllegalArgumentException("回收站不存在该文件!文件名:"+keyword);
    }

    private ALiYunDriveFile getALiYunDriveFile(String keyword) throws IOException {
        List<ALiYunDriveFile> aLiYunDriveFileList = aLiYunDriveAPI.search(keyword);
        System.out.println(aLiYunDriveFileList);
        Assert.assertNotNull(aLiYunDriveFileList);
        Assert.assertEquals(1,aLiYunDriveFileList.size());
        return aLiYunDriveFileList.get(0);
    }
}