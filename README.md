# QuickSDk

QuickSDK是一个开源的SDK包.目前支持以下功能:

## 阿里云盘

* 获取用户信息
* 获取用户可用容量和总空间大小
* 获取阿里云盘文件列表
* 查找用户文件列表
* 创建/重命名/移动/删除文件夹(文件)
* 查看/分享/取消分享文件夹(文件)
* 查看/收藏/取消收藏文件夹(文件)

## 微云

* 获取用户可用容量和总空间大小
* 获取微云文件夹信息
* 创建文件夹
* 删除文件夹
* 下载微云文件
* 解析微云分享链接
* 保存微云分享链接
* 分享微云文件
* 为分享微云文件设置密码
* 获取回收站文件列表
* 清空回收站
......

## 百度云

* 获取用户空间容量信息
* 解析百度云分享链接
* 解析带密码的百度云分享链接
* 下载分享链接
* 保存分享链接
* 获取百度云文件列表
* 创建文件夹
* 删除文件夹
* 私密分享百度云文件
* 公开分享百度云文件
* 取消分享百度云文件
* 重命名文件
* 删除文件
* 查看回收站文件
* 清空回收站
......

QuickSDK需要用户手动设置微云Cookie或者百度云Cookie后才可使用.

## 快速入门

maven引入
```xml
<dependency>
    <groupId>cn.schoolwow</groupId>
    <artifactId>QuickSDK</artifactId>
    <version>1.1</version>
</dependency>
```

```java
WeiYunAPI weiYunAPI = new WeiYunAPIImpl();
weiYunAPI.setCookie("{weiyun.com域名下的所有Cookie信息}");
weiYunAPI.xxx();//调用相关方法

BaiDuYunAPI baiDuYunAPI = new BaiDuYunAPIImpl();
baiDuYunAPI.setCookie("{baidu.com域名下所有Cookie信息}");
baiDuYunAPI.xxx();//调用相关方法

ALiYunDriveAPI aLiYunDriveAPI = new ALiYunDriveAPIImpl("{token信息}");
aLiYunDriveAPI.xxx();//调用相关方法
```

> 由于网盘API可能随时变化,本框架并不能保证随时可用.

# 登录信息如何获取

* 登录百度网盘-按F12打开控制台-选择NETWORK面板-点击某一个请求-复制Cookie信息

![百度Cookie](images/百度网盘.png)

* 登录微云-按F12打开控制台-选择NETWORK面板-点击某一个请求-复制Cookie信息

![微云](images/微云.png)

* 登录阿里云盘-按F12打开控制台-选择NETWORK面板-点击某一个请求-复制Authorization信息

![微云](images/阿里网盘.png)

# 免责声明

本框架仅用于技术交流,请勿使用本框架用于非法目的.因使用者使用不当造成的一切后果与本人无关.

# 反馈

* 提交Issue
* 邮箱: 648823596@qq.com
* QQ群: 958754367(quick系列交流,群初建,人较少)

# 开源协议
本软件使用 [GPL](http://www.gnu.org/licenses/gpl-3.0.html) 开源协议!

非商业使用无限制,若需商业使用,请通过邮箱,QQ群,微信号联系本人获取授权.